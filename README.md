### **Description**
React web app for countdown until chosen date and time in several units.  
Local storage for remembering data for browser is used.  
material-ui, material-ui-pickers and moments libraries are used in this app.

---
### **Link**
[https://cas.znoj.cz/](https://cas.znoj.cz/)

---
### **Technology**
JavaScript, Material-UI, React, GitLab CI/CD

---
### **Year**
2018

---
### **Screenshots**
Main page:  
![](./README/main.png)  
  
Calendar widget:  
![calendar](README/calendar.png)  
  
Phone layout:  
![phone](README/phone.png)  
  

