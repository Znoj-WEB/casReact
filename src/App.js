import React, { PureComponent } from "react";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from './Grid';

const styles = {
  card: {
    margin: 'auto',
    maxWidth: '800px',
  },
  media: {
    height: 0,
    paddingTop: '50%', // 16:9
  },
};

export default withStyles(styles)(class App extends PureComponent {
  state = {
    selectedDate: new Date()
  };

  handleDateChange = (date) => {
    console.log('setting date', date);
    localStorage.setItem('date', date);
    this.setState({ selectedDate: date });
  };

  componentDidMount() {
    console.log('gitlabVersion from 24. 7. 2018');
    const date = localStorage.getItem('date')
    if (date) {
      this.setState({ selectedDate: date });
    }
  }

  render() {
    const { classes } = this.props;
    const { selectedDate } = this.state;

    return (
      <div style={{ 'padding': '10px' }}>
        <Card className={classes.card} raised>
          <CardMedia
            className={classes.media}
            image="y.jpg"
          />
          <CardContent>
            <Grid id={'mainGrid'} selectedDate={selectedDate} selectDate={this.handleDateChange} />
          </CardContent>
          <CardMedia
            className={classes.media}
            image="z.jpg"
          />
        </Card>
      </div>
    );
  }
})
