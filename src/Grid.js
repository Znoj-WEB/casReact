import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import DatePicker from "./DatePicker"
import moment from 'moment'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    minHeight: '32px',
    textAlign: 'center',
    lineHeight: '32px',
    color: theme.palette.text.secondary,
  },
});

function CenteredGrid(props) {
  const { classes } = props;
  const now = new Date();
  const future = moment(new Date(props.selectedDate.valueOf()));
  let substraction = (future - now)
  if (substraction < 0) {
    substraction = 0;
  }
  let s, min, h, d, c, calendar;
  if (substraction > 10000) {
    s = Math.round(substraction / 1000);
    min = Math.round(s / 60);
    h = Math.round(min / 60);
    d = Math.round(h / 24);
    c = moment(future).fromNow();
    let days = future.diff(now, 'days');
    let hours = future.subtract(days, 'days').diff(now, 'hours');
    let minutes = future.subtract(hours, 'hours').diff(now, 'minutes');
    let seconds = future.subtract(minutes, 'minutes').diff(now, 'seconds');

    calendar = `${days} dní, ${hours} hodin, ${minutes} minut, ${seconds} vteřin`;
  }
  else {
    s = min = h = d = c = calendar = '-';
  }
  return (
    <div className={classes.root}>
      <Grid id='titles' container justify={'center'} spacing={24} style={{ padding: '2% 1%' }}>
        <Grid item md={6} xs={12}>
          <Paper id='1_paper' className={classes.paper}>
            <Grid id='1_grid' container>
              <Grid id='1_label' item xs={3}>
                Dnes:
              </Grid>
              <Grid id='1_value' item xs={9}>
                <b>{moment().format('LLL')}</b>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
        <Grid item md={6} xs={12}>
          <Paper id='2_paper' className={classes.paper}>
            <Grid id='2_grid' container>
              <Grid id='2_label' item xs={3}>
                Vyber:
              </Grid>
              <Grid id='2_value' item xs={9}>
                <b><DatePicker selectedDate={props.selectedDate} selectDate={props.selectDate} /></b>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>

      <Grid id='result' container justify={'center'} spacing={24} style={{ padding: '2% 1%' }}>
        {[{ label: 'Zbývá', value: calendar },
        { label: 'Cca', value: c },
        { label: 'Dnů', value: d },
        { label: 'Hodin', value: h },
        { label: 'Minut', value: min },
        { label: 'Vteřin', value: s }].map(function (item) {
          const label = item.label;
          const value = item.value;
          return <Grid id={label} key={label} item md={6} xs={12}>
            <Paper id={`${label}_paper`} className={classes.paper}>
              <Grid id={`${label}_grid`} container>
                <Grid id={`${label}_label`} item xs={2}>
                  {label}:
                </Grid>
                <Grid id={`${label}_value`} item xs={10}>
                  <b>{value}</b>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        })}
      </Grid>
    </div >
  );
}

CenteredGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CenteredGrid);