import React, { PureComponent } from "react";
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from "material-ui-pickers/utils/MuiPickersUtilsProvider";
import DateTimePicker from "material-ui-pickers/DateTimePicker";
import moment from 'moment';
import 'moment/locale/cs';

export default class DatePicker extends PureComponent {
  render() {
    return (
      <MuiPickersUtilsProvider utils={MomentUtils} moment={moment} locale="cs">
        <div className="pickers">
          <DateTimePicker
            disablePast
            value={this.props.selectedDate}
            onChange={this.props.selectDate}
            showTodayButton
            format="DD. MMMM YYYY HH:mm"
          />
        </div>
      </MuiPickersUtilsProvider>
    );
  }
}
